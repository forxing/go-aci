#ifndef ACCI_ST
#define ACCI_ST

#include "ACCIdef.h"

#ifndef ACCICOMMON_ST
#include <acciCommon.h>
#endif

#ifndef ACCIDATA_ST
#include <acciData.h>
#endif

#ifndef ACCICONTROL_ST
#include <acciControl.h>
#endif

// #ifndef ACCIOBJECTS_ST
// #include <occiObjects.h>
// #endif

// #ifndef ACCIAQ_ST
// #include <occiAQ.h>
// #endif

/*---------------------------------------------------------------------------
                     PUBLIC TYPES AND CONSTANTS
  ---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
                     PRIVATE TYPES AND CONSTANTS
  ---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
                           EXPORT FUNCTIONS
  ---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
                          INTERNAL FUNCTIONS
  ---------------------------------------------------------------------------*/


#endif                                              /* ACCI_ST */
